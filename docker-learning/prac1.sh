#Sample Input

#4
#1
#2
#9
#8
#Sample Output

#5.000

#Explanation
#The '4' in the first line indicates that there are four integers whose average is to be computed.

#The average = (1 + 2 + 9 + 8)/4 = 20/4 = 5.000 (correct to three decimal places).

#Please include the zeroes even if they are redundant (e.g. 0.000 instead of 0).

#!/bin/bash
read n
index=0
for a in $(seq 1 $n)
do
read z
arr[index]=$z
index=$((index+1))
done

arrayIndex=0
totalElements=${#arr[*]}
sum=0

for i in ${arr[*]}
do
if [ $arrayIndex -le $totalElements ] ; then
sum=$((arr[arrayIndex]+sum))
arrayIndex=$((arrayIndex+1))
fi
done
printf "%.3f" $(bc -l <<< $sum/$totalElements)