#For each line of input, print its  character on a new line for a total of  lines of output.

#!/bin/bash
read n
index=0
for i in $(seq 1 $n)
do
read input
arr[index]=$input
index=$((index+1))
done

arrIndex=0
total=${#arr[*]}
for i in ${arr[*]}
do
if [ $arrIndex -lt $total ] ; then
echo "${arr[arrIndex]}" | cut -c3
arrIndex=$((arrIndex+1))
fi
done