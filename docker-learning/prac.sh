#!/bin/bash
read n
index=0
while [ "$n" != "stop" ]
do
arr[index]=$n
index=$((index+1))
read n
done
echo "array = ${arr[*]} , total ${#arr[*]}"
arrayIndex=0
totalElements=${#arr[*]}
sum=0
for i in ${arr[*]}
do
if [ $arrayIndex -le $totalElements ] ; then
sum=$((arr[arrayIndex]+sum))
arrayIndex=$((arrayIndex+1))
fi
done
echo $sum
printf "%.3f" $(bc -l <<< $sum/$totalElements)
#echo $avg