#!/bin/bash
args=$#
echo "Number of arguments passed is $args"

if [ -z "$1" ] && [ -z "$2" ] ; then
    startValue=1
    endValue=10
else
    startValue=$1
    if [ -z "$2" ];then
        endValue=10
        if [ $startValue -gt $endValue ]; then
            endValue=$((startValue+10))
        fi
    else
        endValue=$2
    fi    
fi

index=0
while [ $startValue -le $endValue ]
do
arr[index]=$startValue
startValue=$((startValue+1))
index=$((index+1))
done

indexCount=0
for i in ${arr[*]}
do
if [ $indexCount -le ${#arr[*]} ]; then
    echo "${arr[$indexCount]}"
    indexCount=$((indexCount+1))
fi
done