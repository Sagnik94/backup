resource "aws_instance" "MyFirstInstance" {
    count = 1
    #ami = "ami-045137e8d34668746"
    ami = lookup(var.AMIS, var.AWS_REGION)
    instance_type = "t2.micro"

    tags = {
        Name = "DemoInstance-${count.index}"
    }

    security_groups = "${var.Security_Groups}"
}