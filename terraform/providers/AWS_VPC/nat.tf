#define external ip
resource "aws_eip" "levelup_nat" {
    vpc = true
}

resource "aws_nat_gateway" "levelup_nat_gw" {
    allocation_id = aws_eip.levelup_nat.id
    subnet_id = aws_subnet.levelup_vpc_public_1.id
    depends_on = [
      aws_internet_gateway.levelup_gw
    ]
}

#creating route table for private ips
resource "aws_route_table" "levelup_private" {
  vpc_id =aws_vpc.levelup_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    natnat_gateway_id = aws_nat_gateway.levelup_nat_gw.id
  }

  tags = {
    Name = "levelup_private"
  }
}

#creating route table association for private ips
resource "aws_route_table_association" "levelup_private_1_a" {
  subnet_id = aws_subnet.levelup_vpc_private_1.id
  route_table_id = aws_route_table.levelup_private.id
}

resource "aws_route_table_association" "levelup_private_2_a" {
  subnet_id = aws_subnet.levelup_vpc_private_2.id
  route_table_id = aws_route_table.levelup_private.id
}

resource "aws_route_table_association" "levelup_private_3_a" {
  subnet_id = aws_subnet.levelup_vpc_private_3.id
  route_table_id = aws_route_table.levelup_private.id
}