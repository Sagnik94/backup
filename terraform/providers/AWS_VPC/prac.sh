#!/bin/bash

#arr=(1 2 3 4 5)
#echo ${arr[@]}

#echo ${#arr[@]}

#startPoint=$1
#endPoint=$2

a=$#

echo $a
if [ -z $1 ] ; then
startPoint=1
else
startPoint=$1
fi

if [ -z $2 ]; then
endPoint=10
else
endPoint=$2
fi

while [ $startPoint -le $endPoint ] ;
do
arr[$startPoint]=$startPoint
startPoint=$((startPoint+1))
done

echo ${arr[@]}

for count in ${arr[@]}
do
random_number=$(( RANDOM )) 
echo -e "${arr[count]}, $random_number"
count=$((count+1))
done

#string="madam"
read -p "Enter a string: " string
echo $string

reverseString=$(echo $string|rev)
echo $reverseString

if [ $string == $reverseString ]; then
echo "palindrome"
else
echo "Not palindrome"
fi

PS3='please select a choice: '
select opt in a b c
do
    case $opt in
    "a")
    echo -e "found a"
    break
    ;;
    "b")
    echo -e "found b"
    break
    ;;
    "c")
    echo -e "found c"
    break
    ;;
    *)
    echo -e "invalid choice"
    exit 1
    ;;
    esac
done