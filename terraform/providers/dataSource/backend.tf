terraform {
    backend "s3" {
        bucket = "terraform-test-12"
        key = "development/terraform_state"
        region = "us-east-2"
    }
}