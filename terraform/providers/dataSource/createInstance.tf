data "aws_availability_zones" "available" {}

data "aws_ami" "latest_ubuntu" {
    most_recent = true
    owners = ["099720109477"]

    filter {
        name = "name"
        values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
    }

    filter{
        name = "virtualization-type"
        values = ["hvm"]
    }
}

resource "aws_instance" "MyFirstInstance" {
    count = 1
    #ami = lookup(var.AMIS, var.AWS_REGION)
    ami = data.aws_ami.latest_ubuntu.id
    instance_type = "t2.micro"
    availability_zone = data.aws_availability_zones.available.names[0]

    provisioner "local-exec" {
        command = "echo ${aws_instance.MyFirstInstance[count.index].private_ip},  ${aws_instance.MyFirstInstance[count.index].public_ip} >> ips.txt"
    }

    tags = {
        Name = "DemoInstance-${count.index}"
    }
}

#output "My_public_ip" {
#    value = "${aws_instance.MyFirstInstance[count.index].public_ip}"
#}